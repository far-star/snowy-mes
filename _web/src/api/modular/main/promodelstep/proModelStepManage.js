import { axios } from '@/utils/request'

/**
 * 查询产品型号步骤
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
export function proModelStepPage (parameter) {
  return axios({
    url: '/proModelStep/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 产品型号步骤列表
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
export function proModelStepList (parameter) {
  return axios({
    url: '/proModelStep/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加产品型号步骤
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
export function proModelStepAdd (parameter) {
  return axios({
    url: '/proModelStep/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑产品型号步骤
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
export function proModelStepEdit (parameter) {
  return axios({
    url: '/proModelStep/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除产品型号步骤
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
export function proModelStepDelete (parameter) {
  return axios({
    url: '/proModelStep/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出产品型号步骤
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
export function proModelStepExport (parameter) {
  return axios({
    url: '/proModelStep/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
