/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.puororder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.puororder.Result.PuorOrderResult;
import vip.xiaonuo.modular.puororder.entity.PuorOrder;
import vip.xiaonuo.modular.puororder.param.PuorOrderParam;
import java.util.List;

/**
 * 采购订单service接口
 *
 * @author jiaxin
 * @date 2022-07-27 15:27:15
 */
public interface PuorOrderService extends IService<PuorOrder> {

    /**
     * 查询采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    PageResult<PuorOrderResult> page(PuorOrderParam puorOrderParam);

    /**
     * 采购订单列表
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    List<PuorOrder> list(PuorOrderParam puorOrderParam);

    /**
     * 添加采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    void add(PuorOrderParam puorOrderParam);

    /**
     * 删除采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    void delete(List<PuorOrderParam> puorOrderParamList);

    /**
     * 编辑采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
    void edit(PuorOrderParam puorOrderParam);

    /**
     * 查看采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
     PuorOrder detail(PuorOrderParam puorOrderParam);

    /**
     * 导出采购订单
     *
     * @author jiaxin
     * @date 2022-07-27 15:27:15
     */
     void export(PuorOrderParam puorOrderParam);

}
