/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.stockbalance.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.stockbalance.param.StockBalanceParam;
import vip.xiaonuo.modular.stockbalance.service.StockBalanceService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 库存余额控制器
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
@RestController
public class StockBalanceController {

    @Resource
    private StockBalanceService stockBalanceService;

    /**
     * 查询库存余额
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @GetMapping("/stockBalance/page")
    @BusinessLog(title = "库存余额_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(StockBalanceParam stockBalanceParam) {
        return new SuccessResponseData(stockBalanceService.page(stockBalanceParam));
    }

    /**
     * 添加库存余额
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @PostMapping("/stockBalance/add")
    @BusinessLog(title = "库存余额_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(StockBalanceParam.add.class) StockBalanceParam stockBalanceParam) {
            stockBalanceService.add(stockBalanceParam);
        return new SuccessResponseData();
    }

    /**
     * 删除库存余额，可批量删除
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @PostMapping("/stockBalance/delete")
    @BusinessLog(title = "库存余额_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(StockBalanceParam.delete.class) List<StockBalanceParam> stockBalanceParamList) {
            stockBalanceService.delete(stockBalanceParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑库存余额
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @PostMapping("/stockBalance/edit")
    @BusinessLog(title = "库存余额_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(StockBalanceParam.edit.class) StockBalanceParam stockBalanceParam) {
            stockBalanceService.edit(stockBalanceParam);
        return new SuccessResponseData();
    }

    /**
     * 查看库存余额
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @GetMapping("/stockBalance/detail")
    @BusinessLog(title = "库存余额_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(StockBalanceParam.detail.class) StockBalanceParam stockBalanceParam) {
        return new SuccessResponseData(stockBalanceService.detail(stockBalanceParam));
    }

    /**
     * 库存余额列表
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @GetMapping("/stockBalance/list")
    @BusinessLog(title = "库存余额_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(StockBalanceParam stockBalanceParam) {
        return new SuccessResponseData(stockBalanceService.list(stockBalanceParam));
    }

    /**
     * 导出系统用户
     *
     * @author czw
     * @date 2022-07-28 16:41:37
     */
    @Permission
    @GetMapping("/stockBalance/export")
    @BusinessLog(title = "库存余额_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(StockBalanceParam stockBalanceParam) {
        stockBalanceService.export(stockBalanceParam);
    }

}
