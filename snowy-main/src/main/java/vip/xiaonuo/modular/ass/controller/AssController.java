/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.ass.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.ass.param.AssParam;
import vip.xiaonuo.modular.ass.service.AssService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 装配工单控制器
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
@RestController
public class AssController {

    @Resource
    private AssService assService;

    /**
     * 查询装配工单
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @GetMapping("/ass/page")
    @BusinessLog(title = "装配工单_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(AssParam assParam) {
        return new SuccessResponseData(assService.page(assParam));
    }

    /**
     * 添加装配工单
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @PostMapping("/ass/add")
    @BusinessLog(title = "装配工单_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(AssParam.add.class) AssParam assParam) {
            assService.add(assParam);
        return new SuccessResponseData();
    }

    /**
     * 删除装配工单，可批量删除
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @PostMapping("/ass/delete")
    @BusinessLog(title = "装配工单_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(AssParam.delete.class) List<AssParam> assParamList) {
            assService.delete(assParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑装配工单
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @PostMapping("/ass/edit")
    @BusinessLog(title = "装配工单_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(AssParam.edit.class) AssParam assParam) {
            assService.edit(assParam);
        return new SuccessResponseData();
    }

    /**
     * 查看装配工单
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @GetMapping("/ass/detail")
    @BusinessLog(title = "装配工单_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(AssParam.detail.class) AssParam assParam) {
        return new SuccessResponseData(assService.detail(assParam));
    }

    /**
     * 装配工单列表
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @GetMapping("/ass/list")
    @BusinessLog(title = "装配工单_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(AssParam assParam) {
        return new SuccessResponseData(assService.list(assParam));
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-08-30 13:41:34
     */
    @Permission
    @GetMapping("/ass/export")
    @BusinessLog(title = "装配工单_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(AssParam assParam) {
        assService.export(assParam);
    }

}
