package vip.xiaonuo.modular.task.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import vip.xiaonuo.modular.task.entity.Task;

@Data
public class TaskResult extends Task {
    /**
     * 工单编号
     */
    private String workOrderNo;
    @Excel(name="计划时间段")
    private String startEndTime;
    @Excel(name = "实际时间段")
    private  String  factTime;
    /**
     * 产品名称
     */
     private  String proName;
    /**
     * 工单名称
     */
     private String workStepName;
    /**
     * 产品类型名称
     */
     private String proTypeName;

    /**
     * 工单状态
     */
    private Integer workOrderStatus;
    /**
     * 报工权限
     */
    private String sysUserName;
    /**
     * 不良品项
     */
//    private  String mulBadItem;

}
